package com.automationpractice.tests;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.server.handler.SendKeys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.automationpractice.config.Configurations;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestPlaceOrderComplet extends Configurations {

	private static WebDriver driver = null;

	private static String xpathProduct01Image = ".//*[@id='homefeatured']/li[1]/div/div[1]/div";
	private static String xpathProduct01Name = ".//*[@id='homefeatured']/li[1]/div/div[2]/h5/a";
	private static String xpathProduct01Price = ".//*[@id='homefeatured']/li[1]/div/div[2]/div[1]/span";
	
	private static String xpathProduct02Image = ".//*[@id='homefeatured']/li[2]/div/div[1]/div";
	private static String xpathProduct02Name = ".//*[@id='homefeatured']/li[2]/div/div[2]/h5/a";
	private static String xpathProduct02Price = ".//*[@id='homefeatured']/li[2]/div/div[2]/div[1]/span";
	
	private static String xpathMensageSuccessfullyAdded = ".//*[@id='layer_cart']/div[1]/div[1]/h2";
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.setProperty("webdriver.gecko.driver", pathGeckoDriver);
		driver = new FirefoxDriver();
		driver.get(urlSystem);
		driver.manage().window().maximize();
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("document.body.style.zoom='75%'");

		wait = new WebDriverWait(driver, timeWait);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpathProduct02Image)));
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		driver.quit();
	}
	
	@Test
	public void test01LoginSuccess () {
		WebElement btnLogin = driver.findElement(By.cssSelector(".login"));
		
		wait.until(ExpectedConditions.elementToBeClickable(btnLogin));
		btnLogin.click();
		
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("email"))));
		driver.findElement(By.id("email")).sendKeys(emailLogin);
		driver.findElement(By.id("passwd")).sendKeys(passLogin);
		driver.findElement(By.xpath(".//*[@id='SubmitLogin']")).click();
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".account>span")));
		assertTrue("O login n�o foi realizado com sucesso ou foi logado com outro usu�rio.", driver.findElement(By.cssSelector(".account>span")).getText().equals(userLogin));
	}
	
	/*
	 * Adiciona 3 produtos no carrinho atr�ves nos 3 m�todos existentes.
	 * 	OBS.: no momento, est� sendo adicionado apenas um produto, os demais est�o pendentes de implementa��o.
	 */
	@Test
	public void test02AddCartProducts() {
		Actions action = new Actions(driver);
		driver.get(urlSystem);
		
		String nameProduct01 = driver.findElement(By.xpath(xpathProduct02Name)).getText();
		String priceProduct01 = driver.findElement(By.xpath(xpathProduct02Price)).getText();
		String nameProduct02 = driver.findElement(By.xpath(xpathProduct02Name)).getText();
		String priceProduct02 = driver.findElement(By.xpath(xpathProduct02Price)).getText();
		
		WebElement buttonProceedCheckout = driver.findElement(By.cssSelector(".btn.btn-default.button.button-medium>span"));
		WebElement buttonContinueShopping = driver.findElement(By.cssSelector(".continue.btn.btn-default.button.exclusive-medium"));
		
		// Armazena os dados para valida��o do produto que � exibido no modal de "Product successfully added" 
		WebElement productAddedCartName = driver.findElement(By.id("layer_cart_product_title"));
		WebElement productAddedCartPrice = driver.findElement(By.id("layer_cart_product_price"));
		
		WebElement Product01 = driver.findElement(By.xpath(xpathProduct01Image));
		WebElement Product02 = driver.findElement(By.xpath(xpathProduct02Image));
		
		WebElement buttonQuickViewProduct01 = driver.findElement(By.xpath(".//*[@id='homefeatured']/li[1]/div/div[1]/div/a[2]"));
		WebElement buttonAddCartProduct02OverView = driver.findElement(By.xpath(".//*[@id='homefeatured']/li[2]/div/div[2]/div[2]/a[1]/span"));
		
		action.sendKeys(Keys.PAGE_DOWN).perform();
		
		// TODO: Adiciona o 1� Produto no carrinho. Devido a forma que foi implementado o sistema, pode ser necess�rio adpat�-lo para que n�o aconte�a um teste falso/positivo.
//		// Move o cursor do mouse para cima do produto desejado
//		wait.until(ExpectedConditions.elementToBeClickable(Product01));
//		action.moveToElement(Product01).build().perform();
//		wait.until(ExpectedConditions.elementToBeClickable(buttonQuickViewProduct01));
//		buttonQuickViewProduct01.click();
//		WebElement buttonAddCartProduct01Modal = driver.findElement(By.xpath(".//*[@id='add_to_cart']/button/span"));
//		wait.until(ExpectedConditions.elementToBeClickable(buttonAddCartProduct01Modal));
//		buttonAddCartProduct01Modal.click();
//		
//		// Valida se a mensagem de produto adicionado no carrinho foi exibida com sucesso.
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathMensageSuccessfullyAdded)));
//		
//		// Valida se o produto adicionado no carrinho foi o correto.
//		assertTrue("O produto adicionado ao carrinho n�o foi o correto!", productAddedCartName.getText().equals(nameProduct01));
//		assertTrue("O valor total do produto adicionado ao carrinho n�o est� correto! Pre�o encontrado: " + productAddedCartPrice.getText() + " - Pre�o esperado: " + priceProduct01, productAddedCartPrice.getText().equals(priceProduct01));
//
//		buttonContinueShopping.click();
//		wait.until(ExpectedConditions.invisibilityOf(driver.findElement(By.xpath(xpathMensageSuccessfullyAdded))));
//		
		
		// Adiciona o 2� Produto no carrinho
		// Move o cursor do mouse para cima do produto desejado
		wait.until(ExpectedConditions.elementToBeClickable(Product02));
		action.moveToElement(Product02).build().perform();
		
		// Clica no bot�o para adicionar o produto no carrinho
		wait.until(ExpectedConditions.elementToBeClickable(buttonAddCartProduct02OverView));
		buttonAddCartProduct02OverView.click();
		
		// Valida se a mensagem de produto adicionado no carrinho foi exibida com sucesso.
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathMensageSuccessfullyAdded)));
		
		// Valida se o produto adicionado no carrinho foi o correto.
		assertTrue("O produto adicionado ao carrinho n�o foi o correto!", productAddedCartName.getText().equals(nameProduct02));
		assertTrue("O valor total do produto adicionado ao carrinho n�o est� correto! Pre�o encontrado: " + productAddedCartPrice.getText() + " - Pre�o esperado: " + priceProduct02, productAddedCartPrice.getText().equals(priceProduct02));
		
		// Clica no bot�o 'Proceed to checkout'.
		buttonProceedCheckout.click();
		
		// TODO: Adicionar o 3� produto no carrinho
		
		// TODO: Valida��o dos itens adicionados no carrinho na p�gina de "01 .Summary".
		// TODO: Clicar no bot�o 'Proceed to checkout' na p�gina de "01. Summary"
		
		// Como j� foi realizado o teste de Login, n�o ter� necessidade de logar durante o checkout.
		
		// TODO: Realizar o processo na p�gina de "03. Address"
		// 1- Validar se o endere�o de entraga est� selecionado.
		// 2- Clicar no bot�o 'Proceed to checkout'.
		// 2- Validar se foi direcionado para o pr�ximo passo
		
		// TODO: Realizar o processo na p�gina de "04. Shipping" 
		// 1- Aceitar os termos clicando no checkbox.
		// 2- Clicar no bot�o 'Proceed to checkout'
		// 3- Validar se foi direcionado para o pr�ximo passo
		
		// TODO: Realizar o processo na p�gina "05. Payment"
		// 1- Validar se os itens adiconados no carrinho est�o corretos.
		// 2- Clicar no bot�o "Pay by check".
		// 3- Validar se foi direcionado para p�gina seguinte com os detalhes do "Check payment".
		// 4- Confirmar se o valor total do pedido est� sendo exibido corretamente na p�gina "Check payment"
		// 5- Clicar no bot�o "I confirm my order".
		
		// TODO: Validar se o pedido foi realizado com sucesso.
		// 1- Validar se foi exibido a mensagem "Your order on My Store is complete.". 
		// 2- Validar se na p�gina foi exibido o valor total do pedido corretamente.
		
	}

}
